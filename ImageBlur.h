//
//  ImageBlur.h
//  DIY Subtitle
//
//  Created by mun on 5/22/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageBlur : NSObject
- (UIImage*) blur:(UIImage*)theImage;
@end
