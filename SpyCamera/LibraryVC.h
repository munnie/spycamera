//
//  LibraryVC.h
//  SpyCamera
//
//  Created by mun on 8/13/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"
@interface LibraryVC : UIViewController <MWPhotoBrowserDelegate> {
    IBOutlet FUIButton* btPhoto, *btVideo;
    NSMutableArray* _selections;
    NSArray* photoArray;
}
@property (nonatomic, strong) NSMutableArray* photos;
@property (nonatomic, strong) NSMutableArray* thumbs;
@end
