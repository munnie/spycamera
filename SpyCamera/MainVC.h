//
//  MainVC.h
//  SpyCamera
//
//  Created by mun on 8/6/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface MainVC : UIViewController {
    IBOutlet FUIButton* btEnd;

    IBOutlet UIImageView* avatar;
    IBOutlet UIButton* btPhoto, *btVideo, *btSetting;

    BOOL isPhoto, isVideo;

    NSTimer* timerVideo, *timerCamera;

    float originalLight;

    BOOL isRecording;
    IBOutlet UIView* recView;

    BOOL buttonFlashing;
}
@property AVAudioPlayer* audioPlayer;

@end
