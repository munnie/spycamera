//
//  LibraryVC.m
//  SpyCamera
//
//  Created by mun on 8/13/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import "LibraryVC.h"
#import "SDImageCache.h"
#import "MWCommon.h"
#import "Database.h"
#import "mediaObj.h"
#import "PXAlertView.h"
#import "Setting.h"
@interface LibraryVC ()

@end

@implementation LibraryVC

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (![SETTING getPremium]) {
        self.interstitialPresentationPolicy = ADInterstitialPresentationPolicyAutomatic;
    }
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    // Do any additional setup after loading the view.
    btPhoto.buttonColor = [UIColor cloudsColor];
    btPhoto.shadowColor = [UIColor silverColor];
    btPhoto.shadowHeight = 3.0f;
    btPhoto.cornerRadius = 3.0f;
    [btPhoto setTitle:NSLocalizedString(@"Photo", nil) forState:UIControlStateNormal];
    btPhoto.titleLabel.font = [UIFont boldFlatFontOfSize:13];
    [btPhoto setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateNormal];
    [btPhoto setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateHighlighted];

    btVideo.buttonColor = [UIColor cloudsColor];
    btVideo.shadowColor = [UIColor silverColor];
    btVideo.shadowHeight = 3.0f;
    btVideo.cornerRadius = 3.0f;
    [btVideo setTitle:NSLocalizedString(@"Video", nil) forState:UIControlStateNormal];
    btVideo.titleLabel.font = [UIFont boldFlatFontOfSize:13];
    [btVideo setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateNormal];
    [btVideo setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)browsePhoto:(id)sender
{
    NSMutableArray* photos = [[NSMutableArray alloc] init];
    NSMutableArray* thumbs = [[NSMutableArray alloc] init];
    MWPhoto* photo;
    BOOL displayActionButton = YES;
    BOOL displaySelectionButtons = YES;
    BOOL displayNavArrows = YES;
    BOOL enableGrid = YES;
    BOOL startOnGrid = YES;

    photoArray = [DATABASE getAllFile:mediaTypePhoto];
    // Photos

    for (mediaObj* obj in photoArray) {
        photo = [MWPhoto photoWithImage:[UIImage imageWithContentsOfFile:[DATABASE getPhotoPath:obj.name]]];
        photo.caption = obj.date;
        [photos addObject:photo];
        photo = [MWPhoto photoWithImage:[UIImage imageWithContentsOfFile:[DATABASE getThumbPath:obj.name]]];
        [thumbs addObject:photo];
    }
    _thumbs = thumbs;
    _photos = photos;
    // Create browser
    MWPhotoBrowser* browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = displayActionButton;
    browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = NO;
    browser.alwaysShowControls = NO;
    browser.zoomPhotosToFill = YES;

    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = YES;
    [browser setCurrentPhotoIndex:0];

    // Reset selections
    if (displaySelectionButtons) {
        _selections = [NSMutableArray new];
        for (int i = 0; i < photos.count; i++) {
            [_selections addObject:[NSNumber numberWithBool:NO]];
        }
    }

    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nc animated:YES completion:nil];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser*)photoBrowser
{
    return _photos.count;
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser*)photoBrowser photoAtIndex:(NSUInteger)index
{
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser*)photoBrowser thumbPhotoAtIndex:(NSUInteger)index
{
    if (index < _thumbs.count)
        return [_thumbs objectAtIndex:index];
    return nil;
}

//- (MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index {
//    MWPhoto *photo = [self.photos objectAtIndex:index];
//    MWCaptionView *captionView = [[MWCaptionView alloc] initWithPhoto:photo];
//    return [captionView autorelease];
//}

//- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"ACTION!");
//}

- (void)photoBrowser:(MWPhotoBrowser*)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index
{
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (BOOL)photoBrowser:(MWPhotoBrowser*)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index
{
    return [[_selections objectAtIndex:index] boolValue];
}

//- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index {
//    return [NSString stringWithFormat:@"Photo %lu", (unsigned long)index+1];
//}

- (void)photoBrowser:(MWPhotoBrowser*)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected
{
    [_selections replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:selected]];
    NSLog(@"Photo at index %lu selected %@", (unsigned long)index, selected ? @"YES" : @"NO");
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser*)photoBrowser
{
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)photoBrowser:(MWPhotoBrowser*)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index
{
    [PXAlertView showAlertWithTitle:nil
                            message:nil
                        cancelTitle:NSLocalizedString(@"Cancel", nil)
                        otherTitles:@[ NSLocalizedString(@"Export", nil), NSLocalizedString(@"Delete", nil) ]
                         completion:^(BOOL cancelled, NSInteger buttonIndex) {
                             if (cancelled) {
                                 
                             } else {
                                 if(buttonIndex==1)
                                 {
                                     UIImage*image=[UIImage imageWithData:[NSData dataWithContentsOfFile:[DATABASE getPhotoPath:((mediaObj*)[photoArray objectAtIndex:index]).name]]];
                                 UIImageWriteToSavedPhotosAlbum(image, nil, @selector(doneExport) , nil);
                                 }
                                 if(buttonIndex==2)
                                 {
                                     [DATABASE deleteFile:(mediaObj*)[photoArray objectAtIndex:index]];
                                     [_photos removeObjectAtIndex:index];
                                     [_thumbs removeObjectAtIndex:index];
                                     [photoBrowser reloadData];
                                     photoArray = [DATABASE getAllFile:mediaTypePhoto];
                                 }
                            } }];
}

- (void)doneExport
{
    [PXAlertView showAlertWithTitle:nil
                            message:@"Saved"
                        cancelTitle:NSLocalizedString(@"OK", nil)
                         otherTitle:nil
                         completion:^(BOOL cancelled, NSInteger buttonIndex) {
                             if (cancelled) {} }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
