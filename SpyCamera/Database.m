//
//  Database.m
//  SpyCamera
//
//  Created by mun on 8/13/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import "Database.h"
#import "FMDB.h"

@implementation Database
+ (id)sharedInstance
{
    static Database* sharedInstance = nil;
    if (nil != sharedInstance) {
        return sharedInstance;
    }
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sharedInstance = [[Database alloc] init];
        NSFileManager *fmngr = [[NSFileManager alloc] init];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SpyCamera" ofType:@"sqlite"];
        NSError *error;
        if(![fmngr copyItemAtPath:filePath toPath:[NSString stringWithFormat:@"%@/Documents/SpyCamera.sqlite", NSHomeDirectory()] error:&error]) {
            // handle the error
            NSLog(@"Error creating the database");}
    });
    return sharedInstance;
}

- (NSString*)databasePath
{
    return [NSString stringWithFormat:@"%@/Documents/SpyCamera.sqlite", NSHomeDirectory()];
}

- (void)saveImage:(UIImage*)image
{
    // Create paths to output images
    NSString* name = [self currentTime];
    NSString* path = [NSString stringWithFormat:@"Documents/%@.jpeg", name];
    NSString* thumbPath = [NSString stringWithFormat:@"Documents/%@_thumb.jpeg", name];
    NSString* jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:path];
    NSString* jpgThumbPath = [NSHomeDirectory() stringByAppendingPathComponent:thumbPath];

    [UIImageJPEGRepresentation(image, 1.0) writeToFile:jpgPath atomically:YES];
    [UIImageJPEGRepresentation(image, 0.1) writeToFile:jpgThumbPath atomically:YES];

    FMDatabase* database = [FMDatabase databaseWithPath:[self databasePath]];
    [database open];

    [database executeUpdate:@"INSERT into SpyCamera(name,type,date) VALUES(?,?,?)", name, [NSNumber numberWithInt:mediaTypePhoto], [self currentViewTime]];
    [database close];
}

- (void)saveVideo:(UIImage*)image URL:(NSURL*)videoURL
{
    NSString* name = [self currentTime];
    NSString* path = [NSString stringWithFormat:@"Documents/%@.mov", name];
    NSString* thumbPath = [NSString stringWithFormat:@"Documents/%@_thumb.jpeg", name];
    NSString* videoPath = [NSHomeDirectory() stringByAppendingPathComponent:path];
    NSString* jpgThumbPath = [NSHomeDirectory() stringByAppendingPathComponent:thumbPath];
    [UIImageJPEGRepresentation(image, 0.1) writeToFile:jpgThumbPath atomically:YES];

    NSData* videoData = [NSData dataWithContentsOfURL:videoURL];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        
        [videoData writeToFile:videoPath atomically:NO];
    });

    FMDatabase* database = [FMDatabase databaseWithPath:[self databasePath]];
    [database open];

    [database executeUpdate:@"INSERT into SpyCamera(name,type,date) VALUES(?,?,?)", name, [NSNumber numberWithInt:mediaTypeVideo], [self currentViewTime]];
    [database close];
}

- (NSArray*)getAllFile:(mediaType)mediatype
{
    NSMutableArray* array = [[NSMutableArray alloc] init];
    FMDatabase* database = [FMDatabase databaseWithPath:[self databasePath]];
    [database open];

    FMResultSet* results = [database executeQuery:@"select * from SpyCamera where type=?", [NSNumber numberWithInt:mediatype]];
    while ([results next]) {
        mediaObj* obj = [[mediaObj alloc] init];
        obj.ID = [results intForColumn:@"ID"];
        obj.name = [results stringForColumn:@"name"];
        obj.type = [results intForColumn:@"type"];
        obj.date = [results stringForColumn:@"date"];
        [array addObject:obj];
    }
    [database close];
    return array;
}

- (NSString*)getPhotoPath:(NSString*)fileName
{
    NSString* path = [NSString stringWithFormat:@"Documents/%@.jpeg", fileName];
    return [NSHomeDirectory() stringByAppendingPathComponent:path];
}

- (NSString*)getThumbPath:(NSString*)fileName
{
    NSString* path = [NSString stringWithFormat:@"Documents/%@_thumb.jpeg", fileName];
    return [NSHomeDirectory() stringByAppendingPathComponent:path];
}

- (NSString*)getVideoPath:(NSString*)fileName
{
    NSString* path = [NSString stringWithFormat:@"Documents/%@.mov", fileName];
    return [NSHomeDirectory() stringByAppendingPathComponent:path];
}

- (void)deleteFile:(mediaObj*)file
{
    if (file.type == mediaTypeVideo) {
        NSString* path = [NSString stringWithFormat:@"Documents/%@.mov", file.name];
        NSString* thumbPath = [NSString stringWithFormat:@"Documents/%@_thumb.jpeg", file.name];
        NSString* videoPath = [NSHomeDirectory() stringByAppendingPathComponent:path];
        NSString* jpgThumbPath = [NSHomeDirectory() stringByAppendingPathComponent:thumbPath];
        [[NSFileManager defaultManager] removeItemAtPath:videoPath error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:jpgThumbPath error:nil];

        FMDatabase* database = [FMDatabase databaseWithPath:[self databasePath]];
        [database open];
        [database executeUpdate:@"DELETE FROM SPYCAMERA where ID=?", [NSNumber numberWithInt:file.ID]];
        [database close];
    } else if (file.type == mediaTypePhoto) {
        NSString* path = [NSString stringWithFormat:@"Documents/%@.jpeg", file.name];
        NSString* thumbPath = [NSString stringWithFormat:@"Documents/%@_thumb.jpeg", file.name];
        NSString* jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:path];
        NSString* jpgThumbPath = [NSHomeDirectory() stringByAppendingPathComponent:thumbPath];
        [[NSFileManager defaultManager] removeItemAtPath:jpgPath error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:jpgThumbPath error:nil];

        FMDatabase* database = [FMDatabase databaseWithPath:[self databasePath]];
        [database open];
        [database executeUpdate:@"DELETE FROM SPYCAMERA where ID=?", [NSNumber numberWithInt:file.ID]];
        [database close];
    }
}

- (NSString*)currentTime
{
    NSDateFormatter* formatter;
    NSString* dateString;

    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ddMMyyyyHHmmss"];

    dateString = [formatter stringFromDate:[NSDate date]];
    return dateString;
}

- (NSString*)currentViewTime
{
    NSDateFormatter* formatter;
    NSString* dateString;

    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];

    dateString = [formatter stringFromDate:[NSDate date]];
    return dateString;
}

@end
