//
//  AppDelegate.h
//  SpyCamera
//
//  Created by mun on 8/4/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
