//
//  SettingVC.m
//  SpyCamera
//
//  Created by mun on 8/13/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import "SettingVC.h"
#import "Setting.h"
@interface SettingVC ()

@end

@implementation SettingVC

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (![SETTING getPremium]) {
        self.interstitialPresentationPolicy = ADInterstitialPresentationPolicyAutomatic;
    } else {
        viewBuy.hidden = YES;
    }
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor cloudsColor];

    for (UIControl* control in [self.view subviews]) {
        if ([control isKindOfClass:[UILabel class]]) {
            UILabel* label = (UILabel*)control;
            label.font = [UIFont flatFontOfSize:17];
            label.textColor = [UIColor midnightBlueColor];
        }
        if ([control isKindOfClass:[UIView class]]) {
            for (UIControl* control2 in [control subviews]) {
                if ([control2 isKindOfClass:[UITextView class]]) {
                    UITextView* label = (UITextView*)control2;
                    label.font = [UIFont flatFontOfSize:14];
                    label.textColor = [UIColor midnightBlueColor];
                    label.backgroundColor = [UIColor clearColor];
                }
            }
        }
    }

    btMinus.buttonColor = [UIColor peterRiverColor];
    btMinus.shadowColor = [UIColor belizeHoleColor];
    btMinus.shadowHeight = 1.0f;
    btMinus.cornerRadius = 1.0f;
    btMinus.titleLabel.font = [UIFont boldFlatFontOfSize:17];
    [btMinus setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [btMinus setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];

    btPlus.buttonColor = [UIColor peterRiverColor];
    btPlus.shadowColor = [UIColor belizeHoleColor];
    btPlus.shadowHeight = 1.0f;
    btPlus.cornerRadius = 1.0f;
    btPlus.titleLabel.font = [UIFont boldFlatFontOfSize:17];
    [btPlus setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [btPlus setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];

    btPurchase.buttonColor = [UIColor peterRiverColor];
    btPurchase.shadowColor = [UIColor belizeHoleColor];
    btPurchase.shadowHeight = 3.0f;
    btPurchase.cornerRadius = 3.0f;
    btPurchase.titleLabel.font = [UIFont boldFlatFontOfSize:15];
    [btPurchase setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [btPurchase setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];

    btRestore.buttonColor = [UIColor peterRiverColor];
    btRestore.shadowColor = [UIColor belizeHoleColor];
    btRestore.shadowHeight = 3.0f;
    btRestore.cornerRadius = 3.0f;
    btRestore.titleLabel.font = [UIFont boldFlatFontOfSize:15];
    [btRestore setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [btRestore setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];

    [self loadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    productsRequest.delegate = nil;
    [productsRequest cancel];
    productsRequest = nil;
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData
{
    lbInterval.text = [NSString stringWithFormat:@"%i", [SETTING getInterval]];
    if ([SETTING getGesture]) {
        [btGesture setImage:[UIImage imageNamed:@"cb_checked"] forState:UIControlStateNormal];
    } else {
        [btGesture setImage:[UIImage imageNamed:@"cb_pressed"] forState:UIControlStateNormal];
    }
}

- (IBAction)changeGesture:(id)sender
{
    [SETTING saveGesture];
    [self loadData];
}

- (IBAction)minusInterval:(id)sender
{
    int interval = [SETTING getInterval] - 1;
    if (interval < 1)
        interval = 1;
    [SETTING saveInterval:interval];
    [self loadData];
}

- (IBAction)plusInterval:(id)sender
{
    int interval = [SETTING getInterval] + 1;
    if (interval > 60)
        interval = 60;
    [SETTING saveInterval:interval];
    [self loadData];
}

- (IBAction)purchase:(id)sender
{
    NSSet* productIdentifiers = [NSSet
        setWithObjects:@"com.munnie.spycamerapremium", nil];
    productsRequest = [[SKProductsRequest alloc]
        initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (IBAction)restore:(id)sender
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark-- InApp Purchase
- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}

- (void)purchaseMyProduct:(SKProduct*)product
{
    if ([self canMakePurchases]) {
        SKPayment* payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    } else {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:
                                                          @"Purchases are disabled in your device"
                                                            message:nil
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)paymentQueue:(SKPaymentQueue*)queue restoreCompletedTransactionsFailedWithError:(NSError*)error
{
}
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue*)queue
{
}

#pragma mark StoreKit Delegate

- (void)paymentQueue:(SKPaymentQueue*)queue
    updatedTransactions:(NSArray*)transactions
{
    for (SKPaymentTransaction* transaction in transactions) {
        switch (transaction.transactionState) {
        case SKPaymentTransactionStatePurchasing: {
            NSLog(@"Purchasing");
            break;
        }
        case SKPaymentTransactionStatePurchased: {
            if ([transaction.payment.productIdentifier
                    isEqualToString:@"com.munnie.spycamerapremium"]) {
                NSLog(@"Purchased ");
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:
                                                                  @"Purchase is completed succesfully"
                                                                    message:nil
                                                                   delegate:
                                                                       nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                [alertView show];
                @try {
                    viewBuy.hidden = YES;
                }
                @catch (NSException* exception)
                {
                }
                @finally
                {
                }

                [SETTING savePremium];
            }
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
        case SKPaymentTransactionStateRestored: {
            @try {
                viewBuy.hidden = YES;
            }
            @catch (NSException* exception)
            {
            }
            @finally
            {
            }
            [SETTING savePremium];
            NSLog(@"Restored ");
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
        case SKPaymentTransactionStateFailed: {
            NSLog(@"Purchase failed ");
            break;
        }
        default:
            break;
        }
    }
}

- (void)productsRequest:(SKProductsRequest*)request
     didReceiveResponse:(SKProductsResponse*)response
{
    SKProduct* validProduct = nil;
    int count = (int)[response.products count];
    if (count > 0) {
        validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];
        if ([validProduct.productIdentifier
                isEqualToString:@"com.munnie.spycamerapremium"]) {
            [self purchaseMyProduct:[validProducts objectAtIndex:0]];
        }
    } else {
        UIAlertView* tmp = [[UIAlertView alloc]
                initWithTitle:@"Not Available"
                      message:@"No products to purchase"
                     delegate:nil
            cancelButtonTitle:nil
            otherButtonTitles:@"OK", nil];
        [tmp show];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
