//
//  Setting.h
//  SpyCamera
//
//  Created by mun on 8/6/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import <Foundation/Foundation.h>
#define SETTING [Setting sharedInstance]

@interface Setting : NSObject
+ (id)sharedInstance;
- (void)saveInterval:(int)interval;
- (int)getInterval;
- (void)saveGesture;
- (int)getGesture;
- (void)savePremium;
- (int)getPremium;
- (void)savePhotoQuality:(int)quality;
- (int)getPhotoQuality;
- (void)saveVideoQuality:(int)quality;
- (int)getVideoQuality;
@end
