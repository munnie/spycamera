//
//  SettingVC.h
//  SpyCamera
//
//  Created by mun on 8/13/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
@interface SettingVC : UIViewController <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    IBOutlet FUIButton* btMinus, *btPlus;
    IBOutlet UILabel* lbInterval;
    IBOutlet UIButton* btGesture;
    IBOutlet FUIButton* btRestore, *btPurchase;
    IBOutlet UIView* viewBuy;
#pragma mark-- InApp Purchase
    SKProductsRequest* productsRequest;
    NSArray* validProducts;
}
@end
