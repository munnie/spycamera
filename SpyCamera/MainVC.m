//
//  MainVC.m
//  SpyCamera
//
//  Created by mun on 8/6/14.
//  Copyright (c) 2014 mun. All rights reserved.
//
#import "Database.h"
#import "MainVC.h"
#import "Setting.h"
#import "SharkfoodMuteSwitchDetector.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>
#import "SettingVC.h"
#import <AudioToolbox/AudioServices.h>
static void* CapturingStillImageContext = &CapturingStillImageContext;
static void* RecordingContext = &RecordingContext;
static void* SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;
@interface MainVC () <AVCaptureFileOutputRecordingDelegate> {
    SharkfoodMuteSwitchDetector* detector;
    BOOL isSilence;
}
// Session management.
@property (nonatomic) dispatch_queue_t sessionQueue; // Communicate with the session and other session objects on this queue.
@property (nonatomic) AVCaptureSession* session;
@property (nonatomic) AVCaptureDeviceInput* videoDeviceInput;
@property (nonatomic) AVCaptureMovieFileOutput* movieFileOutput;
@property (nonatomic) AVCaptureStillImageOutput* stillImageOutput;

// Utilities.
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, getter=isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic, readonly, getter=isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) BOOL lockInterfaceRotation;
@property (nonatomic) id runtimeErrorHandlingObserver;

@end

@implementation MainVC

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    dispatch_async([self sessionQueue], ^{
		[self addObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:SessionRunningAndDeviceAuthorizedContext];
		[self addObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:CapturingStillImageContext];
		[self addObserver:self forKeyPath:@"movieFileOutput.recording" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:RecordingContext];

		__weak MainVC *weakSelf = self;
		[self setRuntimeErrorHandlingObserver:[[NSNotificationCenter defaultCenter] addObserverForName:AVCaptureSessionRuntimeErrorNotification object:[self session] queue:nil usingBlock:^(NSNotification *note) {
			MainVC *strongSelf = weakSelf;
			dispatch_async([strongSelf sessionQueue], ^{
				// Manually restarting the session since it must have been stopped due to an error.
				[[strongSelf session] startRunning];
			});
		}]];
		[[self session] startRunning];
    });
}

- (void)viewDidAppear:(BOOL)animated
{
    [UIDevice currentDevice].proximityMonitoringEnabled = YES;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    [self startSpy];
    originalLight = [[UIScreen mainScreen] brightness];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [UIDevice currentDevice].proximityMonitoringEnabled = NO;
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [timerCamera invalidate];
    [timerVideo invalidate];
    [self stopRecordVideo];
}

- (void)viewDidDisappear:(BOOL)animated
{
    dispatch_async([self sessionQueue], ^{
		[[self session] stopRunning];
		
		[[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
		[[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
		
		[self removeObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
		[self removeObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
		[self removeObserver:self forKeyPath:@"movieFileOutput.recording" context:RecordingContext];
    });
}

- (void)observeValueForKeyPath:(NSString*)keyPath ofObject:(id)object change:(NSDictionary*)change context:(void*)context
{
    if (context == CapturingStillImageContext) {
        BOOL isCapturingStillImage = [change[NSKeyValueChangeNewKey] boolValue];

        if (isCapturingStillImage) {
        }
    } else if (context == RecordingContext) {
        BOOL iisRecording = [change[NSKeyValueChangeNewKey] boolValue];

        dispatch_async(dispatch_get_main_queue(), ^{
			if (iisRecording)
			{
				
			}
			else
			{
			
            }
        });
    } else if (context == SessionRunningAndDeviceAuthorizedContext) {
        BOOL isRunning = [change[NSKeyValueChangeNewKey] boolValue];

        dispatch_async(dispatch_get_main_queue(), ^{
			if (isRunning)
			{
				
			}
			else
			{
				
			}
        });
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (BOOL)isSessionRunningAndDeviceAuthorized
{
    return [[self session] isRunning] && [self isDeviceAuthorized];
}

+ (NSSet*)keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized
{
    return [NSSet setWithObjects:@"session.running", @"deviceAuthorized", nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    btPhoto.layer.cornerRadius=30;
    btVideo.layer.cornerRadius=30;
    btSetting.layer.cornerRadius=30;
    btPhoto.clipsToBounds=YES;
    btVideo.clipsToBounds=YES;
    btSetting.clipsToBounds=YES;
    
    btEnd.buttonColor = [UIColor pomegranateColor];
    btEnd.shadowColor = [UIColor alizarinColor];
    btEnd.shadowHeight = 1.0f;
    btEnd.cornerRadius = 30.0f;
    [btEnd setTitle:NSLocalizedString(@"End", nil) forState:UIControlStateNormal];
    btEnd.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [btEnd setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [btEnd setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];

    avatar.clipsToBounds = YES;
    avatar.layer.cornerRadius = avatar.frame.size.width / 2;
    avatar.layer.borderColor = [UIColor whiteColor].CGColor;
    avatar.layer.borderWidth = 5;
    avatar.layer.masksToBounds = YES;

    isPhoto = YES;
    isVideo = NO;
    [self refreshUI];

    // Create the AVCaptureSession
    AVCaptureSession* session = [[AVCaptureSession alloc] init];
    [self setSession:session];

    // Check for device authorization
    [self checkDeviceAuthorizationStatus];

    // In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs, or connections from multiple threads at the same time.
    // Why not do all of this on the main queue?
    // -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch session setup to the sessionQueue so that the main queue isn't blocked (which keeps the UI responsive).

    dispatch_queue_t sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    [self setSessionQueue:sessionQueue];

    dispatch_async(sessionQueue, ^{
		[self setBackgroundRecordingID:UIBackgroundTaskInvalid];
		
		NSError *error = nil;
		
		AVCaptureDevice *videoDevice = [MainVC deviceWithMediaType:AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionBack];
		AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
		
		if (error)
		{
			NSLog(@"%@", error);
		}
		
		if ([session canAddInput:videoDeviceInput])
		{
			[session addInput:videoDeviceInput];
			[self setVideoDeviceInput:videoDeviceInput];
		}
		
		AVCaptureDevice *audioDevice = [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] firstObject];
		AVCaptureDeviceInput *audioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error];
		
		if (error)
		{
			NSLog(@"%@", error);
		}
		
		if ([session canAddInput:audioDeviceInput])
		{
			[session addInput:audioDeviceInput];
		}
		
		AVCaptureMovieFileOutput *movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
		if ([session canAddOutput:movieFileOutput])
		{
			[session addOutput:movieFileOutput];
			AVCaptureConnection *connection = [movieFileOutput connectionWithMediaType:AVMediaTypeVideo];
			if ([connection isVideoStabilizationSupported])
				[connection setEnablesVideoStabilizationWhenAvailable:YES];
			[self setMovieFileOutput:movieFileOutput];
		}
		
		AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
		if ([session canAddOutput:stillImageOutput])
		{
			[stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
			[session addOutput:stillImageOutput];
			[self setStillImageOutput:stillImageOutput];
		}
    });

    [self.session startRunning];
    isRecording = NO;

    recView.clipsToBounds = YES;
    recView.layer.cornerRadius = recView.frame.size.width / 2;
    recView.backgroundColor = [UIColor pomegranateColor];
    recView.layer.borderWidth = 0;
    recView.layer.masksToBounds = YES;
    recView.alpha = 0;
    buttonFlashing = NO;

    if ([SETTING getGesture]) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Hold the phone near your ear (like having a phone call) to start taking photos" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)startFlashingbutton
{
    if (buttonFlashing)
        return;
    buttonFlashing = YES;
    recView.alpha = 0.0f;
    [UIView animateWithDuration:0.5
        delay:0.0
        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionAllowUserInteraction
        animations:^{
                          recView.alpha = 1.0f;
        }
        completion:^(BOOL finished) {
                       // Do nothing
                   }];
}

- (void)stopFlashingbutton
{
    if (!buttonFlashing)
        return;
    buttonFlashing = NO;
    [UIView animateWithDuration:0.12
        delay:0.0
        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState
        animations:^{
                         recView.alpha = 0.0f;
        }
        completion:^(BOOL finished) {
                       // Do nothing
                   }];
}

- (void)capture
{
    if ([self shouldStart]) {
        [self startFlashingbutton];
        dispatch_async([self sessionQueue], ^{
		// Flash set to Auto for Still Capture
		[MainVC setFlashMode:AVCaptureFlashModeOff forDevice:[[self videoDeviceInput] device]];
		
		// Capture a still image.
            @try {
                [[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                    
                    if (imageDataSampleBuffer)
                    {
                        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                        UIImage *image = [[UIImage alloc] initWithData:imageData];
                        
                        [DATABASE saveImage:image];
                        [self stopFlashingbutton];
                    }
                }];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
        });
    }
}

- (BOOL)shouldStart
{
    if (![UIDevice currentDevice].proximityMonitoringEnabled)
        return YES;
    if ([UIDevice currentDevice].proximityState)
        return YES;
    else {
        if ([SETTING getGesture])
            return NO;
        else
            return YES;
    }
}

- (void)recordVideo
{
    if ([self shouldStart]) {
        [self startRecord];
    } else {
        [self stopRecordVideo];
    }
}

- (void)setupAudioSession
{

    static BOOL audioSessionSetup = NO;
    if (audioSessionSetup) {
        return;
    }
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    //UInt32 doSetProperty = 1;

    //AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryMixWithOthers, sizeof(doSetProperty), &doSetProperty);

    AVAudioSession* session = [AVAudioSession sharedInstance];

    NSError* setCategoryError = nil;
    if (![session setCategory:AVAudioSessionCategoryPlayAndRecord
                  withOptions:AVAudioSessionCategoryOptionMixWithOthers
                        error:&setCategoryError]) {
        // handle error
    }

    [[AVAudioSession sharedInstance] setActive:YES error:nil];

    audioSessionSetup = YES;
}

- (BOOL)deviceIsSilenced
{
    return YES;
}

- (void)playStartRecordSound
{
    [self.session stopRunning];
    // [self setupAudioSession];

    static SystemSoundID soundIDStart = 0;
    if (soundIDStart == 0) {
        NSString* path = [[NSBundle mainBundle] pathForResource:@"begin_record" ofType:@"caf"];
        NSURL* filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundIDStart);
    }
    AudioServicesPlaySystemSound(soundIDStart);

    // AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    [self.session startRunning];
}

- (void)playStopRecordSound
{
    [self.session stopRunning];
    [self setupAudioSession];
    static SystemSoundID soundIDStop = 0;
    if (soundIDStop == 0) {
        NSString* path = [[NSBundle mainBundle] pathForResource:@"end_record" ofType:@"caf"];
        NSURL* filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundIDStop);
    }
    AudioServicesPlaySystemSound(soundIDStop);
    [self.session startRunning];
}

- (void)startRecord
{
    [self startFlashingbutton];
    if (isRecording == NO) {
        dispatch_async([self sessionQueue], ^{
		if (![[self movieFileOutput] isRecording])
		{
            [self playStartRecordSound];
            isRecording=YES;
			[self setLockInterfaceRotation:YES];
			
			if ([[UIDevice currentDevice] isMultitaskingSupported])
			{
				// Setup background task. This is needed because the captureOutput:didFinishRecordingToOutputFileAtURL: callback is not received until AVCam returns to the foreground unless you request background execution time. This also ensures that there will be time to write the file to the assets library when AVCam is backgrounded. To conclude this background execution, -endBackgroundTask is called in -recorder:recordingDidFinishToOutputFileURL:error: after the recorded file has been saved.
				[self setBackgroundRecordingID:[[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil]];
			}
			
			// Turning OFF flash for video recording
			[MainVC setFlashMode:AVCaptureFlashModeOff forDevice:[[self videoDeviceInput] device]];
			
			// Start recording to a temporary file.
			NSString *outputFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[[self currentTime] stringByAppendingPathExtension:@"mov"]];
			[[self movieFileOutput] startRecordingToOutputFileURL:[NSURL fileURLWithPath:outputFilePath] recordingDelegate:self];
		}
		else
		{
			
		}
        });
    }
}

- (NSString*)currentTime
{
    NSDateFormatter* formatter;
    NSString* dateString;

    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ddMMyyyyHHmmss"];

    dateString = [formatter stringFromDate:[NSDate date]];
    return dateString;
}

+ (void)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice*)device
{
    if ([device hasFlash] && [device isFlashModeSupported:flashMode]) {
        NSError* error = nil;
        if ([device lockForConfiguration:&error]) {
            [device setFlashMode:flashMode];
            [device unlockForConfiguration];
        } else {
            NSLog(@"%@", error);
        }
    }
}

+ (AVCaptureDevice*)deviceWithMediaType:(NSString*)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray* devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice* captureDevice = [devices firstObject];

    for (AVCaptureDevice* device in devices) {
        if ([device position] == position) {
            captureDevice = device;
            break;
        }
    }

    return captureDevice;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshUI
{
    //btVideo.enabled = [SETTING getPremium];
    if (isPhoto) {
        [btPhoto setImage:[UIImage imageNamed:@"camera_selected"] forState:UIControlStateNormal];
        [btVideo setImage:[UIImage imageNamed:@"record_normal"] forState:UIControlStateNormal];
    } else {
        [btPhoto setImage:[UIImage imageNamed:@"camera_normal"] forState:UIControlStateNormal];
        [btVideo setImage:[UIImage imageNamed:@"record_selected"] forState:UIControlStateNormal];
    }
}

- (IBAction)selectPhoto:(id)sender
{
    if (isPhoto)
        return;
    isPhoto = YES;
    isVideo = NO;
    [self refreshUI];
    [self startSpy];
}

- (IBAction)selectVideo:(id)sender
{
    if ([SETTING getPremium]) {
        if (isVideo)
            return;
        isPhoto = NO;
        isVideo = YES;
        [self refreshUI];
        [self startSpy];
    } else {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"You have to upgrade to Pro version to record video. Go to setting page to upgrade!" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)setting:(id)sender
{
    UIStoryboard* storyboard =
        [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    SettingVC* myVC = (SettingVC*)
        [storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
    [self presentViewController:myVC animated:NO completion:nil];
}

- (void)startSpy
{
    isRecording = NO;
    [timerCamera invalidate];
    [timerVideo invalidate];
    [self stopRecordVideo];
    if (isPhoto) {
        timerCamera = [NSTimer scheduledTimerWithTimeInterval:[SETTING getInterval]
                                                       target:self
                                                     selector:@selector(capture)
                                                     userInfo:nil
                                                      repeats:YES];
        [timerCamera fire];
    } else if (isVideo) {
        timerVideo = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                      target:self
                                                    selector:@selector(recordVideo)
                                                    userInfo:nil
                                                     repeats:YES];
        [timerVideo fire];
    }
}

- (void)stopRecordVideo
{
    [self stopFlashingbutton];
    if (isRecording) {
        [[self movieFileOutput] stopRecording];
    }
}

- (void)checkDeviceAuthorizationStatus
{
    NSString* mediaType = AVMediaTypeVideo;

    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
		if (granted)
		{
			//Granted access to mediaType
			[self setDeviceAuthorized:YES];
		}
		else
		{
			//Not granted access to mediaType
			dispatch_async(dispatch_get_main_queue(), ^{
				[[[UIAlertView alloc] initWithTitle:@"Uh oh!"
											message:@"SpyCam doesn't have permission to use Camera, please change privacy settings"
										   delegate:self
								  cancelButtonTitle:@"OK"
								  otherButtonTitles:nil] show];
				[self setDeviceAuthorized:NO];
			});
		}
    }];
}

#pragma mark File Output Delegate

- (void)captureOutput:(AVCaptureFileOutput*)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL*)outputFileURL fromConnections:(NSArray*)connections error:(NSError*)error
{
    [self playStopRecordSound];
    if (error) {
        [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];

        NSLog(@"%@", error);
        isRecording = NO;
        return;
    }

    [self setLockInterfaceRotation:NO];

    // Note the backgroundRecordingID for use in the ALAssetsLibrary completion handler to end the background task associated with this recording. This allows a new recording to be started, associated with a new UIBackgroundTaskIdentifier, once the movie file output's -isRecording is back to NO — which happens sometime after this method returns.
    [self generateImage:outputFileURL];
}
- (void)generateImage:(NSURL*)url
{
    UIBackgroundTaskIdentifier backgroundRecordingID = [self backgroundRecordingID];
    [self setBackgroundRecordingID:UIBackgroundTaskInvalid];
    AVURLAsset* asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator* generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = TRUE;
    CMTime thumbTime = CMTimeMakeWithSeconds(0, 30);

    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError* error) {
        if (result != AVAssetImageGeneratorSucceeded) {
            NSLog(@"couldn't generate thumbnail, error:%@", error);[[NSFileManager defaultManager] removeItemAtURL:url error:nil];

            isRecording=NO;
            return;
        }
        UIImage*thumbImg=[UIImage imageWithCGImage:im];
        [DATABASE saveVideo:thumbImg URL:url];
        [[NSFileManager defaultManager] removeItemAtURL:url error:nil];
        if (backgroundRecordingID != UIBackgroundTaskInvalid)
			[[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
        isRecording = NO;
    };

    CGSize maxSize = CGSizeMake(320, 180);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
}
- (IBAction)end:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dealloc
{
    [self.session stopRunning];
    self.session = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
