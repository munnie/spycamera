//
//  Database.h
//  SpyCamera
//
//  Created by mun on 8/13/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import <Foundation/Foundation.h>
#define DATABASE [Database sharedInstance]
#import "mediaObj.h"
typedef enum mediaType {
    mediaTypePhoto,
    mediaTypeVideo
} mediaType;

@interface Database : NSObject
+ (id)sharedInstance;
- (void)saveImage:(UIImage*)image;
- (void)saveVideo:(UIImage*)image URL:(NSURL*)videoURL;
- (void)deleteFile:(mediaObj*)file;
- (NSArray*)getAllFile:(mediaType)mediatype;
- (NSString*)getPhotoPath:(NSString*)fileName;
- (NSString*)getThumbPath:(NSString*)fileName;
- (NSString*)getVideoPath:(NSString*)fileName;
@end
