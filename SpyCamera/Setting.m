//
//  Setting.m
//  SpyCamera
//
//  Created by mun on 8/6/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import "Setting.h"

@implementation Setting
+ (id)sharedInstance
{
    static Setting* sharedInstance = nil;
    if (nil != sharedInstance) {
        return sharedInstance;
    }
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sharedInstance = [[Setting alloc] init];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![defaults objectForKey:@"Interval"]) {
            [defaults setObject:@"5" forKey:@"Interval"];
            [defaults setObject:@"1" forKey:@"Gesture"];
            [defaults setObject:@"0" forKey:@"Premium"];
            [defaults setObject:@"2" forKey:@"PhotoQuality"];
            [defaults setObject:@"2" forKey:@"VideoQuality"];
        }
    });
    return sharedInstance;
}

- (void)saveInterval:(int)interval
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:interval] forKey:@"Interval"];
}

- (int)getInterval
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"Interval"] intValue];
}

- (void)saveGesture
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:(1 - [self getGesture])] forKey:@"Gesture"];
}

- (int)getGesture
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"Gesture"] intValue];
}

- (void)savePremium
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:1] forKey:@"Premium"];
}

- (int)getPremium
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"Premium"] intValue];
}

- (void)savePhotoQuality:(int)quality
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:quality] forKey:@"PhotoQuality"];
}

- (int)getPhotoQuality
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"PhotoQuality"] intValue];
}

- (void)saveVideoQuality:(int)quality
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:quality] forKey:@"VideoQuality"];
}

- (int)getVideoQuality
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"VideoQuality"] intValue];
}
@end
