//
//  HomeVC.m
//  SpyCamera
//
//  Created by mun on 8/4/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import "HomeVC.h"
#import "Setting.h"
@interface HomeVC ()

@end

@implementation HomeVC

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewDidAppear:(BOOL)animated
{
    if (firstTime) {
        firstTime = NO;
     //   [self showintro];
    } else {
        firstTime = NO;
        if (![SETTING getPremium]) {
            self.interstitialPresentationPolicy = ADInterstitialPresentationPolicyManual;
            [self requestInterstitialAdPresentation];
        }
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    firstTime = YES;
    [self customControl];
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar configureFlatNavigationBarWithColor:[UIColor belizeHoleColor]];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.title = @"";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
}
- (void)showintro
{
    //STEP 1 Construct Panels
    MYIntroductionPanel* panel = [[MYIntroductionPanel alloc] initWithimage:[UIImage imageNamed:@"guide"] title:@"" description:NSLocalizedString(@"Don't forget to mute the sound by switch off the volume control on the phone side", nil)];
    /*A more customized version*/
    MYIntroductionView* introductionView = [[MYIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) headerText:NSLocalizedString(@"Warning", nil) panels:@[ panel ] languageDirection:MYLanguageDirectionLeftToRight];
    [introductionView setBackgroundImage:[UIImage imageNamed:@"back_blur"]];

    [introductionView.BackgroundImageView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [introductionView.HeaderImageView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [introductionView.HeaderLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [introductionView.HeaderView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [introductionView.PageControl setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [introductionView.SkipButton setAutoresizingMask:UIViewAutoresizingFlexibleWidth];

    //Set delegate to self for callbacks (optional)
    //introductionView.delegate = self;

    //STEP 3: Show introduction view
    [introductionView showInView:self.view animateDuration:0.0];
}

- (void)customControl
{
    btStart.buttonColor = [UIColor cloudsColor];
    btStart.shadowColor = [UIColor silverColor];
    btStart.shadowHeight = 3.0f;
    btStart.cornerRadius = 3.0f;
    [btStart setTitle:NSLocalizedString(@"Start", nil) forState:UIControlStateNormal];
    btStart.titleLabel.font = [UIFont boldFlatFontOfSize:13];
    [btStart setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateNormal];
    [btStart setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateHighlighted];

    btLibrary.buttonColor = [UIColor cloudsColor];
    btLibrary.shadowColor = [UIColor silverColor];
    btLibrary.shadowHeight = 3.0f;
    btLibrary.cornerRadius = 3.0f;
    [btLibrary setTitle:NSLocalizedString(@"Library", nil) forState:UIControlStateNormal];
    btLibrary.titleLabel.font = [UIFont boldFlatFontOfSize:13];
    [btLibrary setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateNormal];
    [btLibrary setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateHighlighted];

    btSetting.buttonColor = [UIColor cloudsColor];
    btSetting.shadowColor = [UIColor silverColor];
    btSetting.shadowHeight = 3.0f;
    btSetting.cornerRadius = 3.0f;
    [btSetting setTitle:NSLocalizedString(@"Setting", nil) forState:UIControlStateNormal];
    btSetting.titleLabel.font = [UIFont boldFlatFontOfSize:13];
    [btSetting setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateNormal];
    [btSetting setTitleColor:[UIColor belizeHoleColor] forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
