//
//  VideoLibrary.m
//  SpyCamera
//
//  Created by mun on 8/13/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import "VideoLibrary.h"
#import "Database.h"
#import "mediaObj.h"
#import "PXAlertView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Setting.h"
@interface VideoLibrary ()

@end

@implementation VideoLibrary

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (![SETTING getPremium]) {
        self.interstitialPresentationPolicy = ADInterstitialPresentationPolicyAutomatic;
    }
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    videoArray = [DATABASE getAllFile:mediaTypeVideo];
    self.tableView.backgroundColor = [UIColor cloudsColor];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [videoArray count];
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"videoCell" forIndexPath:indexPath];

    mediaObj* obj = [videoArray objectAtIndex:indexPath.row];
    cell.imageView.image = [[UIImage imageWithContentsOfFile:[DATABASE getThumbPath:(obj.name)]] imageScaledToSize:CGSizeMake(50, 50)];
    cell.textLabel.text = obj.date;
    cell.textLabel.font = [UIFont boldFlatFontOfSize:14];
    cell.textLabel.textColor = [UIColor midnightBlueColor];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    [PXAlertView showAlertWithTitle:nil
                            message:nil
                        cancelTitle:NSLocalizedString(@"Cancel", nil)
                        otherTitles:@[ NSLocalizedString(@"Export", nil), NSLocalizedString(@"Delete", nil), NSLocalizedString(@"Watch", nil) ]
                         completion:^(BOOL cancelled, NSInteger buttonIndex) {
                             if (cancelled) {
                                 
                             } else {
                                 if(buttonIndex==1)
                                 {
                                     NSURL*movieURL=[NSURL fileURLWithPath:[DATABASE getVideoPath:((mediaObj*)[videoArray objectAtIndex:indexPath.row]).name]];
                                     [self export:movieURL];
                                 }
                                 if(buttonIndex==2)
                                 {
                                     [DATABASE deleteFile:(mediaObj*)[videoArray objectAtIndex:indexPath.row]];
                                     videoArray = [DATABASE getAllFile:mediaTypeVideo];
                                     [self.tableView reloadData];
                                 }
                                 if(buttonIndex==3)
                                 {
                                     NSURL*movieURL=[NSURL fileURLWithPath:[DATABASE getVideoPath:((mediaObj*)[videoArray objectAtIndex:indexPath.row]).name]];
                                     [self watchVideo:movieURL];
                                 }
                             } }];
}

- (void) export:(NSURL*)movieURL
{
    ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [library writeVideoAtPathToSavedPhotosAlbum:movieURL
                                completionBlock:^(NSURL* assetURL, NSError* error) {
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                }];
}

- (void)watchVideo:(NSURL*)movieURL
{
    MPMoviePlayerViewController* moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:movieURL];
    [self.navigationController pushViewController:moviePlayer animated:YES];
}

- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 56)];
    view.backgroundColor = [UIColor belizeHoleColor];
    UIButton* btBack = [[UIButton alloc] initWithFrame:CGRectMake(10, 15, 35, 35)];
    [btBack addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [btBack setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    // [view addSubview:btBack];
    return view;
}

- (void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
