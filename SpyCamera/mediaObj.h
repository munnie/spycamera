//
//  mediaObj.h
//  SpyCamera
//
//  Created by mun on 8/13/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface mediaObj : NSObject
@property int ID;
@property (retain, nonatomic) NSString* name;
@property (retain, nonatomic) NSString* date;
@property int type;

@end
