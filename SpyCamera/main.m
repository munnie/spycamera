//
//  main.m
//  SpyCamera
//
//  Created by mun on 8/4/14.
//  Copyright (c) 2014 mun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
